class CreateCats < ActiveRecord::Migration
  def change
    create_table :cats do |t|
      t.integer :age, presence: true
      t.date :birth_date, presence: true
      t.string :color, presence: true, inclusion: { in: %w(red blue black tan white green plaid)}
      t.string :name, presence: true
      t.string :sex, presence: true, inclusion: { in: ['M', 'F']}
      t.text :description
      
      t.timestamps
    end
  end
end
