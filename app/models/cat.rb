class Cat < ActiveRecord::Base
  validates :age, numericality: true, presence: true
  validates :birth_date, :name, presence: true
  validates :color, inclusion: %w(red blue black tan white green plaid), presence: true
  # validates :name, presence: true
  validates :sex, inclusion: %w(M F), presence: true
  
  has_many(:cat_rental_requests, dependent: :destroy)
end