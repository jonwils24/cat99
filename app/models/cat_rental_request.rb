class CatRentalRequest < ActiveRecord::Base
  belongs_to(:cat)
  validates :cat_id, :status, :start_date, :end_date, presence: true
  validates :status, inclusion: %w(APPROVED PENDING DENIED)
  validate :approval_conflict?
  
  def after_initialize
    self.status ||= 'PENDING'
  end
  
  def overlapping_requests
    conflicts = CatRentalRequest
      .where("cat_id = ?", self.cat_id)
      .where("(? BETWEEN start_date AND end_date) OR 
              (? BETWEEN start_date AND end_date) OR
              (start_date BETWEEN ? AND ?) OR 
              (end_date BETWEEN ? AND ?)",
              self.start_date, self.end_date,
              self.start_date, self.end_date,
              self.start_date, self.end_date 
              )
    conflicts = conflicts.where("id <> ?", self.id) unless self.id.nil?
    conflicts
  end
  
  def overlapping_approved_requests
    overlapping_requests.where("status = 'APPROVED'")
  end
  
  def approval_conflict?
    return false if overlapping_approved_requests.empty? || self.status != 'APPROVED'
    errors[:cat_id] << "already rented for that time frame"
  end
end

