class CatRentalRequestsController < ApplicationController
  def new
    @request = CatRentalRequest.new
    render :new
  end
  
  def create
    @request = CatRentalRequest.new(cat_rental_request_params)
    @cat = Cat.find(@request.cat_id)
    
    if @request.save
      redirect_to cat_url(@cat)
    else
      render :new
    end
  end
  
  
  private
  def cat_rental_request_params
    params.require(:cat_rental_request).permit(:cat_id, :status, :start_date, :end_date)
  end
end
