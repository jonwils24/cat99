class CatsController < ApplicationController
  
  def create
    @cat = Cat.new(cat_params)
    if @cat.save
      redirect_to cat_url(@cat)
    else
      render :new
    end
      
  end
  
  def index
    @cats = Cat.all
  end
  
  def show
    @cat = Cat.find(params[:id])
  end
  
  def update
    @cat = Cat.find(params[:id])
    if @cat.update(cat_params)
      redirect_to cat_url(@cat)
    else
      render :edit
    end
  end
  
  def edit
    @cat = Cat.find(params[:id])
    render :edit
  end
  
  def new
    @cat = Cat.new
    render :new
  end
  
  private
  
  def cat_params
    cat_attrs = [:name, :age, :sex, :description, :color, :birth_date]
    params.require(:cat).permit(*cat_attrs)
  end
end
